package com.guestmasterweb.tomcat.upload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

public class Upload extends HttpServlet {
	private static final long serialVersionUID = -3256848665271283520L;
	private static final Runtime runtime = Runtime.getRuntime(); 
	private String command;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		File temp = File.createTempFile("upload", ".xml"); 
		try(FileOutputStream out = new FileOutputStream(temp)){
			IOUtils.copy(req.getInputStream(), out);
			
			System.out.println("Running " + command + " " + temp);
			
			runtime.exec(command + " " + temp);
			
			resp.setStatus(200);
		} catch (IOException e){
			resp.setStatus(404);
		}
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
	    super.init(config);
	    command = config.getInitParameter("script");
	    if(command == null || command.isEmpty())
	    	throw new ServletException("Recieve script not configured!");
	}
}
